#include <stm32f3xx.h>

#define Low_CS (GPIOE->BRR |= GPIO_BRR_BR_3)
#define High_CS (GPIOE->BSRRH |= GPIO_BSRR_BS_3)

void SPI_Init() {
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOEEN;
	
	GPIOA->MODER |= (GPIO_MODER_MODER5_1 | GPIO_MODER_MODER6_1 | GPIO_MODER_MODER7_1); 
	
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR5 | GPIO_OSPEEDER_OSPEEDR6 | GPIO_OSPEEDER_OSPEEDR7;
	
	GPIOA->AFR[0] |= (5 << (4*5)) | (5 << (4*6)) | (5 << (4*7));
	
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
	
  SPI1->CR1 |= SPI_CR1_BR_1 | SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI;
	
	SPI1->CR2 |= SPI_CR2_DS_2 | SPI_CR2_DS_1 | SPI_CR2_DS_0 | SPI_CR2_FRXTH; 
	
	SPI1->CR1 |= SPI_CR1_SPE;
	
	GPIOE->MODER |= GPIO_MODER_MODER3_0; //Init CS
	
	High_CS;
}

void SPI_SendByte(uint8_t byteToSend) {
	while (!(SPI1->SR & SPI_SR_TXE));
	SPI1->DR = byteToSend;
	while (!(SPI1->SR & SPI_SR_RXNE));
}

void SPI_Write(uint8_t address, uint8_t dataToWrite) {
	Low_CS;
	SPI_SendByte(address);
	SPI_SendByte(dataToWrite);
	High_CS;
}

uint8_t SPI_Read(uint8_t address) {
	uint8_t result;
	Low_CS;
	SPI_SendByte(address);
	
	while (!(SPI1->SR & SPI_SR_TXE));
	result = SPI1->DR;
	while (!(SPI1->SR & SPI_SR_RXNE));
	
	High_CS;
	return result;
}

void Init() {
	SPI_Init();
}

void Delay(uint32_t nCount){
  while(nCount--);
}

int main(void){
	uint8_t res;
	Init();
	SPI_Write(0x20, 0x0F);
  	res = SPI_Read(0x8F);
}
