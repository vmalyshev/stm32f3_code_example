#include <stm32f3xx.h>

void send_to_uart(uint8_t data) {
    while (!(USART1->ISR & USART_ISR_TXE));
    USART1->TDR = (data & 0xFF);
}

void USART1_IRQHandler(void) {
	if (!(USART1->ISR & USART_ISR_RXNE)) {

		RCC->AHBENR |= RCC_AHBENR_GPIOEEN;
		GPIOE->MODER |= GPIO_MODER_MODER10_0;
		GPIOE->ODR |= GPIO_ODR_10;

		USART1->ISR &= ~USART_ISR_RXNE;
	}
}

int main() {
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	GPIOA->MODER |= GPIO_MODER_MODER9_1 | GPIO_MODER_MODER10_1;
	GPIOA->OTYPER &= ~GPIO_OTYPER_OT_9;
	GPIOA->OTYPER &= ~GPIO_OTYPER_OT_10;
	GPIOA->AFR[1] |= ((7 << (4 * 1)) | (7 << (4 * 2)));

	RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
	USART1->BRR = 0x4E2;//0x271; // 15 000 000 / 9600 / 2;

	USART1->CR1 |= USART_CR1_TE;
	USART1->CR1 |= USART_CR1_RE;
	USART1->CR1 |= USART_CR1_UE;
	USART1->CR1 |= USART_CR1_RXNEIE;

  NVIC_EnableIRQ(USART1_IRQn);

	while(1) {
		Delay(0x3FFFF);
		send_to_uart('1');
		Delay(0x3FFFF);
	}
}